import pandas as pd
import nltk 
import re
from nltk.stem import wordnet # to perform lemmitization
from sklearn.feature_extraction.text import TfidfVectorizer # to perform tfidf
from nltk import pos_tag # for parts of speech
from sklearn.metrics import pairwise_distances # to perfrom cosine similarity
from nltk import word_tokenize # to create tokens

# function that performs text normalization steps

def text_normalization(text):
    text=str(text).lower() # text to lower case
    spl_char_text=re.sub(r'[^ a-z]','',text) # removing special characters
    tokens=nltk.word_tokenize(spl_char_text) # word tokenizing
    lema=wordnet.WordNetLemmatizer() # intializing lemmatization
    tags_list=pos_tag(tokens,tagset=None) # parts of speech
    lema_words=[]   # empty list 
    for token,pos_token in tags_list:
        if pos_token.startswith('V'):  # Verb
            pos_val='v'
        elif pos_token.startswith('J'): # Adjective
            pos_val='a'
        elif pos_token.startswith('R'): # Adverb
            pos_val='r'
        else:
            pos_val='n' # Noun
        lema_token=lema.lemmatize(token,pos_val) # performing lemmatization
        lema_words.append(lema_token) # appending the lemmatized token into a list
    
    return " ".join(lema_words) # returns the lemmatized tokens as a sentence 

df=pd.read_excel('./data/dialog_talk_agent.xlsx')

df.ffill(axis = 0,inplace=True) # fills the null value with the previous value.
df['lemmatized_text']=df['Contexto'].apply(text_normalization)

# returns all the unique word from data with a score of that word
tfidf=TfidfVectorizer()
x_tfidf=tfidf.fit_transform(df['lemmatized_text']).toarray() # transforming the data into array
df_tfidf=pd.DataFrame(x_tfidf,columns=tfidf.get_feature_names()) 


# defining a function that returns response to query using tf-idf
def chat_tfidf(text):
    lemma=text_normalization(text) # calling the function to perform text normalization
    tf=tfidf.transform([lemma]).toarray() # applying tf-idf
    cos=1-pairwise_distances(df_tfidf,tf,metric='cosine') # applying cosine similarity
    index_value=cos.argmax() # getting index value 
    return df['Response texto'].loc[index_value]
